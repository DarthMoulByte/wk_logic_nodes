import bpy

from wk_logic_nodes import Utils

__all__ = ['LN_Tree']

@Utils.RegisteredClass
class LN_Tree(bpy.types.NodeTree):
    '''This is the tree holding the BGE Logic Nodes.'''
    bl_idname = 'LogicNodesTree'
    bl_label = 'BGE Logic Nodes Tree'
    bl_icon = 'LOGIC'
