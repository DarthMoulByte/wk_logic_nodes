import nodeitems_utils

from wk_logic_nodes import Utils
from .LN_Tree import LN_Tree

__all__ = ['LN_NodeCategory']

class LN_NodeCategory(object):
    '''This category ensure that it only displays for the Logic Nodes Tree.'''

    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == LN_Tree.bl_idname

    NodeCategories = []

    def __init__(self, identifier, name, description="", items=[], register=True, color=None):
        self.identifier = identifier
        self.name = name

        self.description = description

        self.itemList = []
        self.itemList.extend(items)

        self.color = (.4, .4, .4) if color is None else color

        # Registering the created category
        if register:
            self.__class__.NodeCategories.append(self)

    def items(self, context):
        for item in self.itemList:
            if item.poll is None or context is None or item.poll(context):
                yield item

    def addNodeItem(self, *args, **kargs):
        def decorator(node):
            Utils.RegisteredClass(node)
            self.itemList.append(
                nodeitems_utils.NodeItem(node.bl_idname, *args, **kargs)
            )
            return node
        return decorator
