bl_info = {
    'name': 'Logic Nodes',
    'description': 'Design game logic with nodes.',
    'author': 'WKnight02',
    'version': (0, 0, 0),
    'blender': (2, 7, 8),
    'location': 'NodeEditor',
    'warning': 'Still experimental...',
    'category': 'Game Engine'
}

import bpy
import nodeitems_utils

DEBUG = 'warning' in bl_info
NODE_CATEGORY = 'wk_logic_nodes'

from wk_logic_nodes import Log
from wk_logic_nodes import Loader
from wk_logic_nodes import Utils
from wk_logic_nodes import sockets
from wk_logic_nodes import nodes
from wk_logic_nodes import ln

def register():
    Loader.reload(Log, Loader, Utils, ln, sockets, nodes)
    for cls in Utils.RegisteredClasses:
        Log.d('Registering class {}'.format(cls))
        bpy.utils.register_class(cls)
    nodeitems_utils.register_node_categories(NODE_CATEGORY,
        ln.LN_NodeCategory.LN_NodeCategory.NodeCategories)

def unregister():
    for cls in Utils.RegisteredClasses:
        Log.d('Unregistering class {}'.format(cls))
        bpy.utils.unregister_class(cls)
    nodeitems_utils.unregister_node_categories(NODE_CATEGORY)

if __name__ == '__main__':
    register()
