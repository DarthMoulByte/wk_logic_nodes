import sys

from wk_logic_nodes import DEBUG

def w(*objects, tag=None, fd=sys.stdout, end='\n', flush=True):
    head = '{{{}}} '.format(tag) if tag else ''
    tail = ' '.join(objects)
    fd.write('{}{}{}'.format(head, tail, end))
    if flush: fd.flush()

def e(*args, fd=sys.stderr, **kargs):
    return w(*args, fd=fd, **kargs)

def d(*args, **kargs):
    return e(*args, **kargs) if DEBUG else None
