import bpy

from wk_logic_nodes import Utils
from .LN_SimpleSocket import *

__all__ = ['LN_ObjectSocket']

@Utils.RegisteredClass
class LN_ObjectSocket(LN_SimpleSocket):
    '''Represents a game object'''
    bl_idname = 'LN_ObjectSocket'
    bl_label = 'Object Socket'

    def draw_color(self, context, node):
        return (1, 0, 0, 1)
