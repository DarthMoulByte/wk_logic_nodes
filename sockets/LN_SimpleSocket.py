import bpy

__all__ = ['LN_SimpleSocket']

class LN_SimpleSocket(bpy.types.NodeSocket):
    '''Basic socket'''
    bl_idname = 'LN_SimpleSocket'
    bl_label = 'Simple Socket'

    def draw(self, context, layout, node, text):
        text = text.title()
        layout.label(text)

    def draw_color(self, context, node):
        return (1, 1, 1, 1)
