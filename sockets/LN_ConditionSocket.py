import bpy

from wk_logic_nodes import Utils
from .LN_InversableSocket import *

__all__ = ['LN_ConditionSocket']

@Utils.RegisteredClass
class LN_ConditionSocket(LN_InversableSocket):
    '''Represents a variable'''
    bl_idname = 'LN_ConditionSocket'
    bl_label = 'Condition Socket'

    inverted = bpy.props.BoolProperty(default=False)

    def draw_color(self, context, node):
        return (1, 1, 0, 1)
