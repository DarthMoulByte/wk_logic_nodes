import bpy

from wk_logic_nodes import Utils
from .LN_SimpleSocket import LN_SimpleSocket

__all__ = ['LN_ParameterSocket']

@Utils.RegisteredClass
class LN_ParameterSocket(LN_SimpleSocket):
    '''Represents a variable'''
    bl_idname = 'LN_ParameterSocket'
    bl_label = 'Parameter Socket'

    def draw_color(self, context, node):
        return (.2, .2, 1, 1)
