import bpy

from wk_logic_nodes.ln.LN_Node import *
from . import LogicCategory

@LogicCategory.addNodeItem(label='AND', settings={'operator': repr('AND')})
@LogicCategory.addNodeItem(label='OR', settings={'operator': repr('OR')})
@LogicCategory.addNodeItem(label='XOR', settings={'operator': repr('XOR')})
@LogicCategory.addNodeItem(label='NOT', settings={'operator': repr('NOT')})
class LN_LogicOperatorNode(LN_Node):
    bl_idname = 'LN_LogicOperatorNode'
    bl_label = 'Logic Operator'

    operator = bpy.props.EnumProperty(items=[
        ('AND', 'AND', 'AND operator'),
        ('OR', 'OR', 'OR operator'),
        ('XOR', 'XOR', 'XOR operator'),
        ('NOT', 'NOT', 'NOT operator'),
    ])

    def init(self, context):
        self.inputs.new('LN_ConditionSocket', 'event')
        self.outputs.new('LN_ConditionSocket', 'event')
        self.set_color(LogicCategory.color)

    def draw_buttons(self, context, layout):
        layout.prop(self, 'operator')
        if self.operator != 'NOT':
            for socket in self.inputs[:-1]:
                if not socket.is_linked:
                    self.inputs.remove(socket)
            if self.inputs[-1].is_linked:
                self.inputs.new('LN_ConditionSocket', 'event')
        else:
            for socket in self.inputs[1:]:
                self.inputs.remove(socket)
