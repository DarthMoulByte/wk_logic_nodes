import bpy

from wk_logic_nodes.ln.LN_Node import *
from . import FlowCategory

__all__ = ['LN_ConditionalOperatorNode']

@FlowCategory.addNodeItem(label='Conditional')
class LN_ConditionalOperatorNode(LN_Node):
    bl_idname = 'LN_ConditionalOperatorNode'
    bl_label = 'Conditional Operator'

    def init(self, context):
        self.inputs.new('LN_EventSocket', 'event')
        self.inputs.new('LN_ConditionSocket', 'condition')
        self.outputs.new('LN_EventSocket', 'event')
        self.set_color(FlowCategory.color)
