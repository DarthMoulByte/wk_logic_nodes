from wk_logic_nodes.ln.LN_NodeCategory import *

FlowCategory = LN_NodeCategory('LN_FlowCategory', 'Flow', 'The flow control operators', color=(.4, .2, .4))
