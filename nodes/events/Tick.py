import bpy

from wk_logic_nodes.ln.LN_Node import *
from . import EventCategory

@EventCategory.addNodeItem(label='Tick')
class LN_TickEventNode(LN_Node):
    bl_idname = 'LN_TickEventNode'
    bl_label = 'Tick Event'

    def init(self, context):
        self.inputs.new('NodeSocketIntUnsigned', 'skip')
        self.outputs.new('LN_EventSocket', 'event')
        self.set_color(EventCategory.color)
